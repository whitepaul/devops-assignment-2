import pandas as pd


def describe_data():
    # Create a dictionary with data
    data = {
        "Name": ["Alice", "Bob", "Charlie", "David", "Emily"],
        "Age": [25, 30, 28, 22, 32],
        "City": ["New York", "Los Angeles", "Chicago", "Seattle", "Miami"],
        "Profession": ["Doctor", "Engineer", "Teacher", "Lawyer", "Accountant"]
    }

    # Create a DataFrame from the dictionary
    df = pd.DataFrame(data)

    # Print the first 5 rows
    print("First 5 rows:")
    print(df.head())

    # Print information about the dataframe (data types, non-null values, etc.)
    print("\nDataframe information:")
    print(df.info())

    # Get descriptive statistics for numeric columns
    print("\nDescriptive statistics:")
    print(df.describe())

    # Count the occurrences of each value in the 'Profession' column
    print("\nProfession counts:")
    print(df['Profession'].value_counts())


if __name__ == '__main__':
    describe_data()
